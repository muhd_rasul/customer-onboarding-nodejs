const mysql = require('mysql');
const express = require('express');
var app = express();
const bodyparser = require('body-parser');

app.use(bodyparser.json());

var mysqlConnection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '1234',
    database: 'CustomerDB',
    multipleStatements: true
});

mysqlConnection.connect((err) => {
    if (!err)
        console.log('DB connection succeded.');
    else
        console.log('DB connection failed \n Error : ' + JSON.stringify(err, undefined, 2));
});


app.listen(3000, () => console.log('Express server is runnig at port no : 3000'));


//Get all Customers
app.get('/Customers', (req, res) => {
    mysqlConnection.query('SELECT * FROM Customer', (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});

//Get an Customers
app.get('/Customers/:id', (req, res) => {
    mysqlConnection.query('SELECT * FROM Customer WHERE ID = ?', [req.params.id], (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});

//Delete an Customers
app.delete('/Customers/:id', (req, res) => {
    mysqlConnection.query('DELETE FROM Customer WHERE ID = ?', [req.params.id], (err, rows, fields) => {
        if (!err)
            res.send('Deleted successfully.');
        else
            console.log(err);
    })
});

//Insert an Customers
app.post('/Customers', (req, res) => {
    let cust = req.body;
    var sql = "SET @ID = ?;SET @FirstName = ?;SET @LastName = ?;SET @Age = ?; \
    CALL CustomerAddOrEdit(@ID,@FirstName,@LastName,@Age);";
    mysqlConnection.query(sql, [cust.ID, cust.FirstName, cust.LastName, cust.Age], (err, rows, fields) => {
        if (!err)
            rows.forEach(element => {
                if(element.constructor == Array)
                res.send('Inserted Customer id : '+element[0].ID);
            });
        else
            console.log(err);
    })
});

//Update an Customers
app.put('/Customers', (req, res) => {
    let cust = req.body;
    var sql = "SET @ID = ?;SET @FirstName = ?;SET @LastName = ?;SET @Age = ?; \
    CALL CustomerAddOrEdit(@ID,@FirstName,@LastName,@Age);";
    mysqlConnection.query(sql, [cust.ID, cust.FirstName, cust.LastName, cust.Age], (err, rows, fields) => {
        if (!err)
            res.send('Updated successfully');
        else
            console.log(err);
    })
});
